package projects;

import java.util.Scanner;

public class MicroCalculator {

    public static void main(String[] args) {

        while(true) {

            Scanner scan = new Scanner(System.in);

            System.out.print("Enter First Number : ");
            double x = scan.nextDouble();

            System.out.print("Enter Second Number : ");
            double y = scan.nextDouble();
            while(true) {
                System.out.print("Choose Operator ( + , - , * , / ) : ");
                char operator = scan.next().charAt(0);

                String operatorName = getOperatorName(operator);

                double result;
                if (operatorName.equals("Addition")) {
                    result = x + y;
                    System.out.println("Result of (Sum) is " + result);
                } else if (operatorName.equals("Subtraction")) {
                    result = x - y;
                    System.out.println("Result of (Subtract) is " + result);
                } else if (operatorName.equals("Multiplication")) {
                    result = x * y;
                    System.out.println("Result of (Multiply) is " + result);
                } else if (operatorName.equals("Division")) {
                    result = x / y;
                    System.out.println("Result of (Division) is " + result);
                } else {  // if returnValue == "Invalid"
                    continue;
                }

                break;
            }

            System.out.print("Do you want continue other operation (Y/N)");
            String wantToContinue = scan.next();
            if (!wantToContinue.equalsIgnoreCase("y")){
                break;
            }

        }

    }

    private static String getOperatorName(char symbol){
        String returnValue="";

        switch(symbol){
            case '+' : returnValue = "Addition";
                break;

            case '-' : returnValue = "Subtraction";
                break;

            case '*' : returnValue = "Multiplication";
                break;

            case '/' : returnValue = "Division";
                break;

            default : returnValue= "Invalid";


        }

        return returnValue;
    }
}
