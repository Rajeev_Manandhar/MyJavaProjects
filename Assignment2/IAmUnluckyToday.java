package projects;

public class IAmUnluckyToday {
    public static void main(String[] args) {

        String[] students = new String[6];
        students[0] = "Nabin Limbu";
        students[1]=  "Prabin lamichane";
        students[2] = "Ashish Shrestha";
        students[3] = "Bikram Bhadel";
        students[4] = "Rajeev Manandhar";
        students[5] = "Deepesh Maharjan";

        int number = getRandomNumber();
        System.out.println("random Unlucky Student picked is "+ students[number]);


    }

    // returns an Integer from 0 to 5
    public static int getRandomNumber(){

        double randomDouble = Math.random()*6;
        int number = (int) randomDouble; // downcasting double to Integer. will omit everything after decimal(.) and assign double to int
        System.out.println(number);
        return number;

    }
}
